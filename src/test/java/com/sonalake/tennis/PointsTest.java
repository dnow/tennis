package com.sonalake.tennis;

import static org.junit.Assert.*;

import org.junit.Test;

public class PointsTest {

	@Test
	public void testUp() {
		assertEquals(Points.P15, Points.P0.up());
		assertEquals(Points.P30, Points.P15.up());
		assertEquals(Points.P40, Points.P30.up());
		assertEquals(Points.PA, Points.P40.up());
	}
	
	@Test
	public void testDown() {
		assertEquals(Points.P40, Points.PA.down());
	}
	
	@Test
	public void testDiff() {
		assertEquals(0, Points.PA.diff(Points.PA));
		assertEquals(1, Points.P40.diff(Points.P30));
		assertEquals(2, Points.P40.diff(Points.P15));
	}

}
