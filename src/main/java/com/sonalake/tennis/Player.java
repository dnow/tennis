package com.sonalake.tennis;

public enum Player {
	SERVER("Player 1"),
	RECEIVER("Player 2");
	
	private String name;

	private Player(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Player getOpponent() {
		return values()[Math.abs(this.ordinal() - 1)];
	}

}
