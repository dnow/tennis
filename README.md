#Tennis score game
Console program which can be used to track the score of a tennis game.

##Prerequisites
- Java 8

## Used technologies
- Java 8
- junit

##Build process
`mvn clean install`
 
jar can be found inside target dir
 
##Usage

`java -jar target/tennis-0.0.1-SNAPSHOT-jar-with-dependencies.jar`

run below for color version
`java -jar target/tennis-0.0.1-SNAPSHOT-jar-with-dependencies.jar c` 

##Assumptions

Keep it as simple as possible. Also introduced GameScoreRule abstraction in game_score branch but it has more code and complexity so left it simple. Please see game_score branch for details.
