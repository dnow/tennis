package com.sonalake.tennis;

public enum Points {
	P0("0"),
	P15("15"),
	P30("30"),
	P40("40"),
	PA("A");
	
	private String representation;

	private Points(String representation) {
		this.representation = representation;
	}
	
	public String getRepresentation() {
		return representation;
	}
	
	public Points up() {
		return values()[this.ordinal() + 1];
	}
	
	public Points down() {
		return values()[this.ordinal() - 1];
	}
	
	public int diff(Points point) {
		return this.ordinal() - point.ordinal();
	}
	
}
