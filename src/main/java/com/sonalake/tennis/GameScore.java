package com.sonalake.tennis;


public class GameScore {
	
	private Points serverPoints = Points.P0;
	
	private Points recevierPoints = Points.P0;
	
	private Player winner;
	
	public void reset() {
		winner = null;
		serverPoints = Points.P0;
		recevierPoints = Points.P0;
	}
	
	public Player getWinner() {
		return this.winner;
	}
	
	protected void setWinner(Player player) {
		this.winner = player;
	}
	
	public Points getPoints(Player player) {
		if(Player.SERVER.equals(player)) {
			return serverPoints;
		}
		return recevierPoints;
	}
	
	public void setPoints(Player player, Points points) {
		if(Player.SERVER.equals(player)) {
			serverPoints = points;
		}else {
			recevierPoints = points;
		}
	}
	
	protected void setPoints(Points serverPoints, Points recevierPoints) {
		this.serverPoints = serverPoints;
		this.recevierPoints = recevierPoints;
	}
	
	public boolean pointWonBy(Player player) {
		if(winner != null) {
			throw new IllegalStateException("Game already won by " + winner.getName());
		}
		Points playerPoints = getPoints(player);
		Points opponentPoints = getPoints(player.getOpponent());
		if(isGamePoint(playerPoints, opponentPoints)) {
			winner = player;
			return true;
		}else if(Points.PA.equals(opponentPoints)) {
			setPoints(player.getOpponent(), opponentPoints.down());
		}else {
			setPoints(player, playerPoints.up());
		}
		return false;
	}
	
	protected boolean isGamePoint(Points playerPoints, Points oponentPoints) {
		return Points.PA.equals(playerPoints)
			|| (Points.P40.equals(playerPoints) && Points.P40.diff(oponentPoints) > 0); 
	}
	
	
	@Override
	public String toString() {
		StringBuilder strB = new StringBuilder();
		if(winner != null) {
			strB.append(winner.getName()).append(" won");
		}else {
			strB.append(serverPoints.getRepresentation()).append(":").append(recevierPoints.getRepresentation());
		}
		return strB.toString();
	}

}
