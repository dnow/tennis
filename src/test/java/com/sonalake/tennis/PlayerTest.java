package com.sonalake.tennis;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void testGetOponent() {
		assertEquals(Player.SERVER, Player.RECEIVER.getOpponent());
		assertEquals(Player.RECEIVER, Player.SERVER.getOpponent());
	}

}
