package com.sonalake.tennis;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import com.sonalake.tennis.GameScore;
import com.sonalake.tennis.Player;
import com.sonalake.tennis.Points;

public class GameScoreTest {
	
	private GameScore gameScore;
	
	@Before
	public void setup() {
		gameScore = new GameScore();
	}
	
	@Test
	public void testGetPoints() {
		//when
		gameScore.pointWonBy(Player.SERVER);
		//then
		assertEquals(Points.P0, gameScore.getPoints(Player.RECEIVER));
		assertEquals(Points.P15, gameScore.getPoints(Player.SERVER));
	}

	@Test
	public void testScoreIncrementCorrectly1() {
		 //Given the score is 0:0, when the server wins a point Then the score is 15:0.
		//given
		gameScore.setPoints(Points.P0, Points.P0);
		//when
		assertFalse(gameScore.pointWonBy(Player.SERVER));
		//then
		assertEquals("15:0", gameScore.toString());
	}
	
	@Test
	public void testScoreIncrementCorrectly2() {
		//Given the score is 0:0 when the receiver wins a point Then the score is 0:15.
		//given
		gameScore.setPoints(Points.P0, Points.P0);
		//when
		assertFalse(gameScore.pointWonBy(Player.RECEIVER));
		//then
		assertEquals("0:15", gameScore.toString());
	}
	
	@Test
	public void testScoreIncrementCorrectly3() {
		//Given the score is 15:15 when the server wins a point Then the score is 30:15.
		//given
		gameScore.setPoints(Points.P15, Points.P15);
		//when
		assertFalse(gameScore.pointWonBy(Player.SERVER));
		//then
		assertEquals("30:15", gameScore.toString());
	}
	
	@Test
	public void testScoreIncrementCorrectly4() {
		//Given the score is 30:30 when the receiver wins a point then the score is 30:40.
		//given
		gameScore.setPoints(Points.P30, Points.P30);
		//when
		assertFalse(gameScore.pointWonBy(Player.RECEIVER));
		//then
		assertEquals("30:40", gameScore.toString());
	}
	
	@Test
	public void testWinnerDeclaredCorrectly1() {
		//Given the score is 40:30 when the server wins the they are declared the winner.
		//given
		gameScore.setPoints(Points.P40, Points.P30);
		//when
		assertTrue(gameScore.pointWonBy(Player.SERVER));
		//then
		assertEquals(Player.SERVER, gameScore.getWinner());
	}
	
	
	@Test
	public void testWinnerDeclaredCorrectly2() {
		//Given the score is 40:A when the receiver wins the point they are declared the winner.
		//given
		gameScore.setPoints(Points.P40, Points.PA);
		//when
		assertTrue(gameScore.pointWonBy(Player.RECEIVER));
		//then
		assertEquals(Player.RECEIVER, gameScore.getWinner());
	}
	
	@Test
	public void testWinnerDeclaredCorrectly3() {
		//Given the score is 40:15 when the server wins the they are declared the winner.
		//given
		gameScore.setPoints(Points.P40, Points.P15);
		//when
		assertTrue(gameScore.pointWonBy(Player.SERVER));
		//then
		assertEquals(Player.SERVER, gameScore.getWinner());
	}

	@Test
	public void testResetScore() {
		//given
		gameScore.pointWonBy(Player.SERVER);
		//when
		gameScore.reset();
		//then
		assertEquals(Points.P0, gameScore.getPoints(Player.SERVER));
		assertEquals("0:0", gameScore.toString());
	}
	
	@Test
	public void testResetAfterWon() {
		//given
		gameScore.setPoints(Points.PA, Points.P40);
		gameScore.pointWonBy(Player.SERVER);
		//when
		gameScore.reset();
		//then
		assertEquals(Points.P0, gameScore.getPoints(Player.SERVER));
		assertEquals("0:0", gameScore.toString());
	}

}
