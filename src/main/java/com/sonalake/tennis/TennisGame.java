package com.sonalake.tennis;

import java.util.Scanner;

public class TennisGame {
	
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	
	private Scanner scanner = new Scanner(System.in);
	
	private GameScore gameScore = new GameScore();
	
	private boolean useColors = false;
	
	public static void main(String[] args) {
		TennisGame tenisGame = new TennisGame();
		if(args.length > 0) {
			tenisGame.useColors = true;
		}
		tenisGame.welcome();
		tenisGame.play();
	}
	
	public void welcome() {
		System.out.println("Server = 1\nReceiver = 2\n");
	}
	
	public void goodbye() {
		print(ANSI_PURPLE, "Goodbye");
	}
	
	public int askForPlayer() {
		System.out.println("Please enter the player that wins the point:");
		return scanner.nextInt();
	}
	
	protected void print(String color, String text) {
		if(useColors) {
			System.out.println(color + text + ANSI_RESET);
		}else {
			System.out.println(text);
		}
	}
	
	public void play() {
		while(true) {
			int player = askForPlayer();
			if(player == 0) {
				goodbye();
				break;
			}else if (player > 0 && player < 3) {
				boolean gameEnd = gameScore.pointWonBy(Player.values()[player - 1]);
				print(ANSI_GREEN, gameScore.toString());
				if(gameEnd) {
					print(ANSI_YELLOW, "Starting new game");
					gameScore.reset();
				}
			}else {
				print(ANSI_RED, "Invalid player number");
			}
		}
	}
	

}
